import warnings
from _warnings import warn
import random

def stable_matching(projects, groups, verbose=False):
    '''
    The stable_matching is the top-level object where all the methods are
    implemented.
    '''
    keys =  list(groups.keys())      # Python 3; use keys = d.keys() in Python 2
    random.shuffle(keys)
#    print(keys)
    out = False
    while any([(not group.assigned_project) for group in groups.values()]):
        
        for key in keys:
            group = groups[key]
            if not group.has_a_project:
                if verbose:
                    print('Group {} is looking for a project.'.format(group))
                for project in group.wishes:
                    if verbose:
                        print('\t He applies to project {}'.format(project))
                    
                    #### group applies for the project ####
                    if len(project.assigned_group) < project.spot_nb:
                        project.assign(group)
                        is_accepted = True
                        break
                    
                    else:
                        # sort group by project priorities
                        project.assigned_group.sort(key=lambda group: project.priorities.get(group, 0), reverse=True)
                        
                        if project.priorities.get(project.assigned_group[-1], 0) < project.priorities.get(group, 0):
                            project.unassign(project.assigned_group[-1])
                            project.assign(group)
                            is_accepted = True
                            break
                        else:
                            is_accepted = False
                    #######################################
                    if verbose:
                        if is_accepted:
                            print('\t -> He is accepted by {}.'.format(project))
                        else:
                            print('\t -> He is not accepted by {}.'.format(project))
                
                else:  # Le else d'une boucle for est appelé lorsque la liste est épuisée avant un break
                    out = True
                    break
#                    raise ValueError('Group {} is refused by its whole wish list'.format(group))
        if out:
            keys = []
            break
    return keys


class SeveralCombinationsAcceptableWarning(UserWarning, ValueError):
    pass


def check_unicity(projects, groups):
    
    other_pairs = list()
    
    for group in groups.values():
        for project in projects.values():
            if group.assigned_project is not project:
                group_strictly_prefer_project = (group.wish_rank(project) <= group.wish_rank(group.assigned_project))
                
                priority_of_least_favorite_assigned_group = min([project.priorities.get(g, 0) for g in project.assigned_group], default=0)
                project_strictly_prefer_group = project.priorities.get(group, 0) >= priority_of_least_favorite_assigned_group
                
                is_matching_pair = group_strictly_prefer_project and project_strictly_prefer_group
                
                if is_matching_pair:
                    other_pairs.append((project, group))
    
    projects_with_multiple_options = dict()
    
    for project, group in other_pairs:
        if project not in projects_with_multiple_options:
            projects_with_multiple_options[project] = [project.assigned_group]
        projects_with_multiple_options[project].append(group)
    
    if other_pairs:
        text = ''
        for project in projects_with_multiple_options:
            text += '{} : \n\t {}\n'.format(project, projects_with_multiple_options[project])
        warnings.warn(
            SeveralCombinationsAcceptableWarning(
                'Please add preferences among the following:\n' + text + '\n'
            )
        )
        return False
    else:
        return True


class AdvisorWithoutStudentWarning(UserWarning, ValueError):
    pass


def get_first_advisor_without_students(projects):
    # Setup advisor dict
    advisors_projects = dict()
    for proj in projects.values():
        if proj.advisor:
            if proj.advisor in advisors_projects:
                advisors_projects[proj.advisor].append(proj)
            else:
                advisors_projects[proj.advisor] = [proj]
     
    for advisor in advisors_projects.keys():
        advisor_has_students = any(proj.assigned_group for proj in advisors_projects[advisor])
        
        if not advisor_has_students:
            warn(AdvisorWithoutStudentWarning(advisor))
            return advisor
    
    else:
        return None


def check_advisor_is_without_students(projects):
    advisor_without_student = get_first_advisor_without_students(projects)
    if advisor_without_student:
        warn(AdvisorWithoutStudentWarning(advisor_without_student))


def print_recap(projects):
    print('<Projects>\n\t<Groups>  \t<[wish rank]>')
    for p in projects.values():
        print("{}\n\t{}  \t{}".format(p.name, p.assigned_group, [group.wish_rank(p)+1 for group in p.assigned_group]) )
        

