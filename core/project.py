

class Project(object):
    '''
    A project will be affected to some students
    
    :param name: Name of the project
    :type name: :class:`str`
    :param spot_nb: Number of students that should work on the project
    :type spot_nb: :class:`int`
    :param priorities: Priority for each group. Negative number is a veto. Highest priority group should be chosen. Defaut priority is 0.
    :type priorities: :class:`dict` of :class:`Group`::class:`int`
    '''
    
    def __init__(self, spot_nb=1, name=None, priorities=None, advisor=None):
        self.spot_nb = spot_nb
        self.name = name
        self.priorities = priorities
        self.assigned_group = list()
        self.advisor = advisor
        self.rank = 0 # informal ranking value
        self.candidates = [] #informal list of candidates
    
    def get_application_from(self, group):
        if len(self.assigned_group) < self.spot_nb:
            self.assign(group)
            return True
        
        else:
            self.assigned_group.sort(key=lambda group: self.priorities.get(group, 0), reverse=True)
                # La méthode get d'un dictionnaire est l'équivalent
                # de objet[key] avec une valeur par défaut si la key
                # n'est pas dans le dictionnaire
            
            if self.priorities.get(self.assigned_group[-1], 0) < self.priorities.get(group, 0):
                self.unassign(self.assigned_group[-1])
                self.assign(group)
                return True
            
            return False
    
    def assign(self, group):
        if len(self.assigned_group) <= self.spot_nb:
            self.assigned_group.append(group)
            group.assigned_project = self
        else:
            raise RuntimeError("Project {} already full, can't assign Group {}".format(self, group))
    
    def unassign(self, group):
        if group in self.assigned_group:
            self.assigned_group.remove(group)
            group.assigned_project = None
        else:
            raise RuntimeError("Group {} not assigned to project {} and can't be unassigned".format(group, self))
    
    def __repr__(self):
        #  return '{0} ({1})'.format(self.name, self.spot_nb)
        return '{0}'.format(self.name)
