

class Group(object):
    '''
    A group of students can choose different projects and order them according to
    their preference.
    
    :param name: Name of the students
    :type name: :class:`set` of :class:`str`
    :param wishes: Ordered list of projects, first element being favored.
    :type wishes: :class:`list` of :class:`Project`
    '''
    
    def __init__(self, name=None, wishes=None):
        '''
        Constructor
        '''
        self.name = name
        self.wishes = wishes
        self.assigned_project = None
    
    def assign(self, project):
        self.assigned_project = project
        project.assigned_group = self
#        print(self.name, ' associés à ', project.name)
    
    def unassign(self):
        if self.assigned_project:
            self.assigned_project.assigned_group = None
        
        self.assigned_project = None
    
    def wish_rank(self, project):
        ''' Returns the index of project in the wish list of the group.
        
        Return infinity if not in the list.
        '''
        index = next(
            (i for i, item in enumerate(self.wishes) if item is project),
            float('inf')
        )
        return index
    
    @property
    def has_a_project(self):
        if self.assigned_project:
            return True
        else:
            return False
    
    def apply_for(self, project):
        return project.get_application_from(self)
    
    def __repr__(self):
        if self.name is None:
            return 'Anonymous'
        else:
            return self.name
