from core.project import Project
from core.group import Group

import json
import csv

def get_from_json(project_file, group_file):
    
    project_list = dict()
    group_list = dict()
    
    with open(project_file) as json_file:
        project_data = json.load(json_file)
    
    with open(group_file) as json_file:
        group_data = json.load(json_file)
    
    ##  Set all values except priorities and wishes
    for project in project_data['projects']:
        project_list[project['id']] = Project(
            name=project['name'],
            spot_nb=project['spots'],
        )
    
    for group in group_data['groups']:
        group_list[group['id']] = Group(
            name=group['students'],
        )
    
    ##  Set up wishes and priorities after
    for project in project_data['projects']:
        priorities = {group_list[key]: value for prio in project['priorities'] for key, value in prio.items()}
        project_list[project['id']].priorities = priorities
    
    for group in group_data['groups']:
        wishes = [project_list[key] for key in group['wishes']]
        group_list[group['id']].wishes = wishes
    
    return project_list, group_list

# %%
def parse_csv_Moodle_wishes_file(filename, projects):
    ''':projects: dict containing Projects() objects'''
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        row = next(reader)
        group_pos = [pos for pos, x in enumerate(row) if x == 'Groupe'][0]
        firstwish_pos = [pos for pos, x in enumerate(row) if x == 'Q01'][0]
        groups = dict()
        for row in reader:
            group_name = row[group_pos]
            wishes_raw = row[firstwish_pos:firstwish_pos + 15]
            wishes_raw = [wish for wish in wishes_raw if wish]
            wishes = [projects[aa] for aa in wishes_raw]
            newgroup = Group(name = group_name, wishes=wishes)
            
            groups[group_name] = newgroup
    
    return groups


def parse_project_list(filename):
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        head = next(reader)
        projects = {}
        for row in reader:
            priors = {val: int(row[i+5]) for i,val in enumerate(head[5:]) if row[i+5]}   
            newproject = Project(name = row[1], advisor = row[2], spot_nb = int(row[3]), priorities=priors)
            projects[row[0]] = newproject
        
    return projects

# %%
def compute_projects_pop_ranking(groups):
    for group in groups.values():
        for rk, project in enumerate(group.wishes):
            project.rank += 15-rk
            
def sort_projects(projects):
    listproj = list(projects.values())
    listproj.sort(key=lambda x: x.rank, reverse=True)
    print('***** Project ranking *****')
    for projet in listproj:
        print('{} \t{:>}'.format(projet, projet.rank))
    
def find_projects_candidates(groups):
    for group in groups.values():
        for rk, project in enumerate(group.wishes):
            project.candidates.append((group, rk+1))
            project.candidates.sort(key=lambda x: x[0].name)
            
            
            
            