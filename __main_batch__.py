#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on June 2018

@author: Association Méristème - https://meristeme.org

"""
import sys, csv

from core.matching import stable_matching, check_unicity, print_recap
from core.utils import parse_csv_Moodle_wishes_file, parse_project_list#, get_from_json

verbose = False

if len(sys.argv) == 1:
#    projectfile = "tests/data/projects.json"
#    groupfile = "tests/data/groups.json"
#    projectfile = "tests/data/projets_proposes_test.csv"
#    wishfile = "tests/data/liste_de_voeux_test.csv"
    projectfile = "../projets_proposes.csv"
#    wishfile = "../Liste_de_voeux.csv"
    wishfile = "../Liste_de_voeux_vrais_binomes.csv"

#    verbose = True
    
elif len(sys.argv) >= 3:
    projectfile = sys.argv[1]
    wishfile = sys.argv[2]
    
    if len(sys.argv) == 4:
        verbose = True

class Sol(object):
    
    def __init__(self, keys=[], projects = None):
        self.keys = keys
        self.projects = projects
        
    def score(self):
        score = 0
        for p in self.projects.values():
            ranks = [group.wish_rank(p)+1 for group in p.assigned_group]
            score += sum([aa**2 for aa in ranks])
        return score 
    
    def sha(self):
        return ''.join([aa[-2:] + '-' for aa in self.keys])
    
    def __repr__(self):
        return '{} \t {}'.format(self.sha(), self.score())

list_sol = []
for i in range(1000):
    #projects, groups = get_from_json(projectfile, groupfile)
    projects = parse_project_list(projectfile)
    groups = parse_csv_Moodle_wishes_file(wishfile, projects)
    
    keys = stable_matching(projects, groups, verbose=verbose)
    sol = Sol(keys=keys, projects = projects)

    if keys and (sol.sha() not in [aa.sha() for aa in list_sol]):
        print(sol)
        list_sol.append(sol)
#        print(list_sol)


list_sol.sort(key= lambda x: x.score())    # sorting solutions by score

list_groups = []
list_wishrank = []
for sol in list_sol[:5]: # 5 best solutions
    # DICT[sol.sha()] = 
    list_groups.append([p.assigned_group for p in sol.projects.values()])
    list_wishrank.append([[group.wish_rank(p)+1 for group in p.assigned_group] for p in sol.projects.values()])

with open('result.csv', 'w') as csvfile:
    spamwriter = csv.writer(csvfile)
    spamwriter.writerow(['Projet', 'Référent',
                         'Prop.1', '',
                         'Prop.2', '',
                         'Prop.3', '',
                         'Prop.4', '',
                         'Prop.5', ''])
    for i,p in enumerate(projects.values()):
        print('{}\n\t{}-{}\t{}-{}\t{}-{}\t{}-{}\t{}-{}'.format(p.name,
              list_groups[0][i], list_wishrank[0][i],
              list_groups[1][i], list_wishrank[1][i],
              list_groups[2][i], list_wishrank[2][i],
              list_groups[3][i], list_wishrank[3][i],
              list_groups[4][i], list_wishrank[4][i])
        )
        spamwriter.writerow([p.name, p.advisor,
                             list_groups[0][i], list_wishrank[0][i],
                             list_groups[1][i], list_wishrank[1][i],
                             list_groups[2][i], list_wishrank[2][i],
                             list_groups[3][i], list_wishrank[3][i],
                             list_groups[4][i], list_wishrank[4][i]])
    print([aa.score() for aa in list_sol[:5]])
    spamwriter.writerow(['', 'Score (à minimiser)',
                         '', list_sol[0].score(), 
                         '', list_sol[1].score(), 
                         '', list_sol[2].score(),
                         '', list_sol[3].score(),
                         '', list_sol[4].score()])
#print(len(list_sol))

#scores = [aa for aa in DICTs.values()]
#scores.sort()
#print(scores[:20])
    
#if check_unicity(projects, groups):
#    print('{:^50}'.format('****** Optimal solution found ******'))
#else:
#    print('{:^50}'.format('!!!!!! A possible solution is !!!!!!'))
#print_recap(projects)
    
#sha = ''
#with open('result.csv', 'w') as csvfile:
#    spamwriter = csv.writer(csvfile, delimiter='\t')
#    for p in projects.values():
##        print(p.assigned_group[:])
#        sha += str(p.assigned_group[-2:])
#        sha += '-'
#        spamwriter.writerow([p.name, p.assigned_group, [group.wish_rank(p)+1 for group in p.assigned_group]])
#print(sha)

# Other data
if len(sys.argv) == 4:
    from core.utils import compute_projects_pop_ranking, sort_projects, find_projects_candidates
    
    compute_projects_pop_ranking(groups)
    sort_projects(projects)
    
    find_projects_candidates(groups)
    print('***** Project candidates *****')
    for project in projects.values():
        print('{} : \n\t {}\n'.format(project, project.candidates))
