import unittest
from core.utils import get_from_json

import sys
sys.path.insert(0, './')
sys.path.insert(0, '../')
sys.path.insert(0, '../../')


class Test_Reading(unittest.TestCase):
    
    def test_read_group_file(self):
        
        projects, groups = get_from_json(
            project_file='data/projects.json',
            group_file='data/groups.json',
        )
        
        self.assertEqual(projects['P1'].name, 'Projet')
        
        self.assertIn('Alice', groups['G1'].name)
        self.assertIn('Alfred', groups['G1'].name)
