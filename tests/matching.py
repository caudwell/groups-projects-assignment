import unittest
from core.project import Project
from core.group import Group
from core.matching import stable_matching, check_unicity,\
    check_advisor_is_without_students, SeveralCombinationsAcceptableWarning
from core.utils import parse_csv_Moodle_wishes_file, parse_project_list

import random

import warnings
from core.matching import AdvisorWithoutStudentWarning

def create_projects_and_groups(number_of_pairs):
    '''
    Create dict of projects with defaults values and groups indexed by Pn and Gn.
    '''
    projects = {'P' + str(i): Project(name='P' + str(i)) for i in range(number_of_pairs)}
    groups = {'G' + str(i): Group(name='G' + str(i)) for i in range(number_of_pairs)}
    return projects, groups


class Test_Matching(unittest.TestCase):

    def test_obvious_case(self):
        '''
        Check that the algorithm gives the expected result on this imaginary case.
        '''
        proj1 = Project(spot_nb=1, name='P1')
        proj2 = Project(spot_nb=1, name='P2')
        proj3 = Project(spot_nb=1, name='P3')
        
        group1 = Group()
        group2 = Group()
        group3 = Group()
        
        group1.wishes = [proj1, proj2, proj3]
        group2.wishes = [proj2, proj3, proj1]
        group3.wishes = [proj3, proj1, proj2]
        
        stable_matching(
            projects={'P1': proj1, 'P2': proj2, 'P3': proj3},
            groups={'G1': group1, 'G2': group2, 'G3': group3}
        )
        
        self.assertIs(group1.assigned_project, proj1)
        self.assertIs(group2.assigned_project, proj2)
        self.assertIs(group3.assigned_project, proj3)
    
    def test_wishlist_exhausted(self):
        '''
        Check that the algorithm gives the expected result on this imaginary case.
        '''
        proj1 = Project(spot_nb=1, name='P1')
        proj2 = Project(spot_nb=1, name='P2')
        proj3 = Project(spot_nb=1, name='P3')
        
        group1 = Group()
        group2 = Group()
        group3 = Group()
        
        group1.wishes = [proj1]
        group2.wishes = [proj1]
        group3.wishes = [proj1]
        
        errors = list()
        try:
            stable_matching(
                projects={'P1': proj1, 'P2': proj2, 'P3': proj3},
                groups={'G1': group1, 'G2': group2, 'G3': group3}
            )
        except Exception as e:
            errors.append(e)
            pass
        
        self.assertTrue(len(errors) > 0)
    
    def test_obvious_case_with_project_priorities(self):
        
        proj1 = Project(spot_nb=1, name='P1')
        proj2 = Project(spot_nb=1, name='P2')
        proj3 = Project(spot_nb=1, name='P3')
        
        group1 = Group()
        group2 = Group()
        group3 = Group()
        
        group1.wishes = [proj1, proj2, proj3]
        group2.wishes = [proj1, proj2, proj3]
        group3.wishes = [proj1, proj2, proj3]
        
        proj1.priorities[group1] = 1
        proj2.priorities[group2] = 1
        proj3.priorities[group3] = 1
        
        stable_matching(
            projects={'P1': proj1, 'P2': proj2, 'P3': proj3},
            groups={'G1': group1, 'G2': group2, 'G3': group3}
        )
        
        self.assertIs(group1.assigned_project, proj1)
        self.assertIs(group2.assigned_project, proj2)
        self.assertIs(group3.assigned_project, proj3)
    
    def test_big_random_case_with_project_priorities(self):
        
        number_of_pairs = 50 # number of associations project/group
        projects, groups = create_projects_and_groups(number_of_pairs)
        
        for group in groups.values():
            group.wishes = random.sample(list(projects.values()), len(projects))
        
        for project in projects.values():
            for group in groups.values():
                project.priorities[group] = random.randint(1, 9)
        
        stable_matching(projects, groups)
        
        for group in groups.values():
            for project in projects.values():
                group_strictly_prefer_project = group.wish_rank(project) < group.wish_rank(group.assigned_project)
                
                priority_of_least_favorite_assigned_group = min([project.priorities.get(g, 0) for g in project.assigned_group])
                project_strictly_prefer_group = project.priorities.get(group, 0) > priority_of_least_favorite_assigned_group
            
                better_pair = group_strictly_prefer_project and project_strictly_prefer_group
                if better_pair:
                    print(group, project)
                self.assertFalse(better_pair)
    
    def test_big_random_case_with_partial_inputs(self):
        
        number_of_pairs = 50
        number_of_priorities = 5
        number_of_wishes = 50
        projects, groups = create_projects_and_groups(number_of_pairs)
        
        for group in groups.values():
            group.wishes = random.sample(list(projects.values()), number_of_wishes)
        
        for project in projects.values():
            for group in random.sample(list(groups.values()), number_of_priorities):
                project.priorities[group] = random.randint(1, 9)
        
        with warnings.catch_warnings(record=True) as w:
            stable_matching(projects, groups)
            check_unicity(projects, groups)
        
        for group in groups.values():
            for project in projects.values():
                if group.assigned_project is not project:
                    group_strictly_prefer_project = (group.wish_rank(project) <= group.wish_rank(group.assigned_project))
                    
                    priority_of_least_favorite_assigned_group = min([project.priorities.get(g, 0) for g in project.assigned_group])
                    project_strictly_prefer_group = project.priorities.get(group, 0) > priority_of_least_favorite_assigned_group
                
                    is_matching_pair = group_strictly_prefer_project and project_strictly_prefer_group
                    
                    if is_matching_pair:
                        self.assertTrue(len(w) > 0)
    
    
#     def test_return_correct_results_with_project_vetos(self):
#         pass
    
    def test_return_correct_results_for_project_with_more_than_one_spot(self):
        proj1 = Project(spot_nb=1, name='P1')
        proj2 = Project(spot_nb=2, name='P2')
        
        group1 = Group()
        group2 = Group()
        group3 = Group()
        
        proj2.priorities[group1] = 1
        proj2.priorities[group2] = 0
        proj2.priorities[group3] = 1
        
        group1.wishes = [proj2, proj1]
        group2.wishes = [proj2, proj1]
        group3.wishes = [proj2, proj1]
        
        stable_matching(
            projects={'P1': proj1, 'P2': proj2},
            groups={'G1': group1, 'G2': group2, 'G3': group3}
        )
        
        self.assertIs(group1.assigned_project, proj2)
        self.assertIs(group2.assigned_project, proj1)
        self.assertIs(group3.assigned_project, proj2)
    
    def test_raise_warning_when_advisor_is_without_students(self):
        proj1 = Project(spot_nb=1, name='P1', advisor='A1')
        proj2 = Project(spot_nb=1, name='P2', advisor='A2')
        proj3 = Project(spot_nb=1, name='P3', advisor='A3')
        proj4 = Project(spot_nb=1, name='P4', advisor='A3')
        proj_list = [proj1, proj2, proj3, proj4]
        
        group1 = Group()
        group2 = Group()
        group3 = Group()
        group1.wishes = [proj2, proj3, proj4]
        group2.wishes = [proj2, proj3, proj4]
        group3.wishes = [proj2, proj3, proj4]
        
        with self.assertWarns(AdvisorWithoutStudentWarning):
            stable_matching(
                projects={'P1': proj1, 'P2': proj2},
                groups={'G1': group1, 'G2': group2, 'G3': group3}
            )
            
            check_advisor_is_without_students(
                projects={'P1': proj1, 'P2': proj2},
            )

    def test_raise_error_when_multiple_solutions(self):
        proj1 = Project(spot_nb=1, name='P1', advisor='A1')
        proj2 = Project(spot_nb=1, name='P2', advisor='A2')
        
        group1 = Group()
        group2 = Group()
        group1.wishes = [proj1, proj2]
        group2.wishes = [proj1, proj2]
        
        with self.assertWarns(SeveralCombinationsAcceptableWarning):
            stable_matching(
                projects={'P1': proj1, 'P2': proj2},
                groups={'G1': group1, 'G2': group2,}
            )
            check_unicity(
                projects={'P1': proj1, 'P2': proj2},
                groups={'G1': group1, 'G2': group2,}
            )

    def test_parsers(self):
        projectfile = 'data/projets_proposes_test.csv'
        wishfile = 'data/liste_de_voeux_test.csv'
        
        projects = parse_project_list(projectfile)
        groups = parse_csv_Moodle_wishes_file(wishfile, projects)
            
        group = groups['Binôme V']
        
        self.assertEqual(group.wishes[0], projects['10'])
        self.assertEqual(group.wishes[9], projects['19'])
        self.assertGreaterEqual(len(group.wishes), 10)


if __name__ == "__main__":
    unittest.main()
