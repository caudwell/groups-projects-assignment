import os.path, sys

test_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.join(test_dir, os.pardir)

sys.path.append(test_dir)
sys.path.append(parent_dir)

os.chdir(test_dir)


import unittest
#  Add here the test modules
from tests.matching import Test_Matching
from tests.json_files import Test_Reading


if __name__ == "__main__":
    #  This runs all the tests it founds.
    unittest.main()
