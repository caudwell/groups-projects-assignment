#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on June 2018

@author: Association Méristème - https://meristeme.org

"""
import sys

from core.matching import stable_matching, check_unicity, print_recap
from core.utils import parse_csv_Moodle_wishes_file, parse_project_list#, get_from_json

verbose = False

if len(sys.argv) == 1:
#    projectfile = "tests/data/projects.json"
#    groupfile = "tests/data/groups.json"
#    projectfile = "tests/data/projets_proposes_test.csv"
#    wishfile = "tests/data/liste_de_voeux_test.csv"
    projectfile = "../projets_proposes.csv"
    wishfile = "../Liste_de_voeux_vrais_binomes.csv"
    verbose = True
    
elif len(sys.argv) >= 3:
    projectfile = sys.argv[1]
    wishfile = sys.argv[2]
    
    if len(sys.argv) == 4:
        verbose = True

#projects, groups = get_from_json(projectfile, groupfile)
projects = parse_project_list(projectfile)
groups = parse_csv_Moodle_wishes_file(wishfile, projects)

stable_matching(projects, groups, verbose=verbose)

if check_unicity(projects, groups):
    print('{:^50}'.format('****** Optimal solution found ******'))
else:
    print('{:^50}'.format('!!!!!! A possible solution is !!!!!!'))
print_recap(projects)

# Other data
if len(sys.argv) == 4:
    from core.utils import compute_projects_pop_ranking, sort_projects, find_projects_candidates
    
    compute_projects_pop_ranking(groups)
    sort_projects(projects)
    
    find_projects_candidates(groups)
    print('***** Project candidates *****')
    for project in projects.values():
        print('{} : \n\t {}\n'.format(project, project.candidates))
