# Algorithme d'affectations de groupes d'étudiants sur des projets

Cet algorithme est dérivé de l'algorithme de Gale-Shapley, permettant de résoudre le [problème dit « des mariages-stables »](https://fr.wikipedia.org/wiki/Probl%C3%A8me_des_mariages_stables).

Il permet d'affecter des groupes d'étudiants à des projets selon des listes de préférences.
Pour chaque projet il est également possible de tenir compte de l'adéquation des groupes à celui-ci: une valeur positive est alors associée à un groupe dont l'adéquation est particulièrement bonne, un valeur négative à un groupe dont l'adéquation est particulièrement mauvaise. Une absence de valeur vaut indifférence.


# Principe

Il prend en entrée:
* une liste de projets, comportant leur nom, leur professeur référent et le nombre de groupes qu'il peut accepter. 
 Optionnellement, des notes peuvent être attribuées aux groupes en fonction de leur adéquation au projet.
* les préférences de chaque groupe via-à-vis des projets proposés 

Son fonctionnement est le suivant:

    Initialisation : aucun groupe g ∈ G n'est affecté à aucun projet p ∈ P
    Tant que ∃ un groupe g qui n'est pas affecté à un projet p {
       p = projet préférée de g parmi ceux à qui il ne s'est pas déjà proposé
       Si p est en mesure d'accepter un groupe
         g est affecté à p
       Sinon si un autre groupe g' est déjà affecté à p
         Si g est en meilleure adéquation avec le projet qu'un des groupes déjà affecté g' (préférences du référent)
            g est affecté à p
            g' se retrouve sans affectation
         Sinon
            on conserve inchangées les affectations à p
    }


# Utilisation

Exécuter le programme `__main__.py` en lui donnant la liste des projets et la liste des voeux en arguments :

    python3 __main__.py <liste_projets.csv> <voeux.csv>
    
# Licence
![logo licence](https://licensebuttons.net/l/by-sa/4.0/88x31.png)
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)
Auteur : [Association Méristème](https://meristeme.org)

    

---

# A retirer

Pour un groupe donné:

    Initialisation : aucun groupe g ∈ G n'est affecté à aucun projet p ∈ P
    Tant que ∃ un groupe g qui n'est pas affecté à un projet p {
       p = projet préférée de g parmi ceux à qui il ne s'est pas déjà proposé
       Si p est affecté à personne
         p est affecté à g
       Sinon si un autre groupe g' est déjà affecté à p
         Si g est plus en adéquation que g' avec le projet (préférences du référent)
            p est affecté à g
            g' se retrouve sans affectation
         Sinon
            p reste affecté à g'
            
Version avec plusieurs groupes possibles:

    Initialisation : aucun groupe g ∈ G n'est affecté à aucun projet p ∈ P
    Tant que ∃ un groupe g qui n'est pas affecté à un projet p {
       p = projet préférée de g parmi ceux à qui il ne s'est pas déjà proposé
       Si p est en mesure d'accepter un groupe
         g est affecté à p
       Sinon si un autre groupe g' est déjà affecté à p
         Si g est en meilleure adéquation avec le projet qu'un des groupes déjà affecté g' (préférences du référent)
            g est affecté à p
            g' se retrouve sans affectation
         Sinon
            on conserve inchangées les affectations à p
    }

    